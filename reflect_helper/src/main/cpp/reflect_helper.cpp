#include <jni.h>
#include <thread>
#include <string>
#include <sstream>
#include <fstream>
#include <future>
#include <android/log.h>

/***
 *
 * 简单介绍就是让系统判断反射调用是系统的而不是APP的，因为安卓源码是通过回溯调用栈，通过调用者的Class来判断是否是系统代码的调用
 * （所有系统的代码都通过BootClassLoader加载，判断ClassLoader即可）。
 * ReflectHelper 库的做法是通过在jni层新建个线程，在这个线程里去反射，去除掉了java调用的信息，从而让安卓系统以为这个是系统调用。
 * */


using namespace std;
JavaVM *_vm = nullptr ;


JNIEnv *attachCurrentThread() {
    JNIEnv *env;
    int res = _vm->AttachCurrentThread(&env, nullptr);
    __android_log_print(ANDROID_LOG_DEBUG, "native", "Found attached %d", res);
    return env;
}

void detachCurrentThread() {
    _vm->DetachCurrentThread();
}


static jobject getDeclaredMethod_internal(jobject clazz, jstring method_name, jobjectArray params) {
    JNIEnv *env = attachCurrentThread();
    jclass clazz_class = env->GetObjectClass(clazz);
    jmethodID get_declared_method_id = env->GetMethodID(clazz_class, "getDeclaredMethod",
                                                        "(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;");
    jobject res = env->CallObjectMethod(clazz, get_declared_method_id, method_name, params);

    jobject globalRes = env->NewGlobalRef(res);
    env->DeleteGlobalRef(clazz) ;
    env->DeleteGlobalRef(method_name) ;

    detachCurrentThread() ;

    return globalRes ;
}


extern "C"
JNIEXPORT jobject JNICALL
Java_com_macoli_reflect_1helper_NativeReflect_getDeclaredMethod(JNIEnv *env, jobject t,
                                                                jclass c, jstring method_name,
                                                                jobjectArray params) {
    std::thread test_thread ;
    jobject global_clazz = env->NewGlobalRef(c) ;
    jstring global_method_name = static_cast<jstring>(env->NewGlobalRef(method_name)) ;
    jobjectArray global_params = nullptr;
    int arg_length = env->GetArrayLength(params);
    if (params != nullptr) {
        for (int i = 0; i < arg_length; i++) {
            jobject element = static_cast<jobject> (env->GetObjectArrayElement(params, i));
            jobject global_element = env->NewGlobalRef(element);
            env->SetObjectArrayElement(params, i, global_element);
        }
        global_params = (jobjectArray) env->NewGlobalRef(params);
    }

    auto future = std::async(&getDeclaredMethod_internal, global_clazz,
                             global_method_name,
                             global_params);
    auto result = future.get();

    if (global_params != nullptr) {
        env->DeleteGlobalRef(global_params) ;
    }

    return result ;
}

static jobject getDeclaredField_internal(jobject object, jstring field_name) {

    JNIEnv *env = attachCurrentThread();

    jclass clazz_class = env->GetObjectClass(object);
    jmethodID methodId = env->GetMethodID(clazz_class, "getDeclaredField",
                                          "(Ljava/lang/String;)Ljava/lang/reflect/Field;");
    jobject res = env->CallObjectMethod(object, methodId, field_name);
    jobject global_res = nullptr;
    if (res != nullptr) {
        global_res = env->NewGlobalRef(res);
    }

    detachCurrentThread();
    return global_res;
}

extern "C"
JNIEXPORT jobject JNICALL
Java_com_macoli_reflect_1helper_NativeReflect_getDeclaredField(JNIEnv *env, jobject t,
                                                               jclass clz, jstring fieldName) {
    auto global_clazz = env->NewGlobalRef(clz);
    jstring global_method_name = static_cast<jstring>(env->NewGlobalRef(fieldName)) ;
    auto future = std::async(&getDeclaredField_internal, global_clazz, global_method_name);
    auto result = future.get();

    env->DeleteGlobalRef(global_clazz) ;
    env->DeleteGlobalRef(global_method_name) ;

    return result ;
}

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    _vm = vm ;
    return JNI_VERSION_1_6;
}

static jobject getDeclaredConstructor_internal(jobject clazz, jobjectArray params) {
    JNIEnv *env = attachCurrentThread();
    jclass clazz_class = env->GetObjectClass(clazz);
    jmethodID get_declared_method_id = env->GetMethodID(clazz_class, "getDeclaredConstructor",
                                                        "([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;");
    jobject res = env->CallObjectMethod(clazz, get_declared_method_id, params);

    jobject globalRes = env->NewGlobalRef(res);
    env->DeleteGlobalRef(clazz) ;

    detachCurrentThread() ;

    return globalRes ;
}

extern "C"
JNIEXPORT jobject JNICALL
Java_com_macoli_reflect_1helper_NativeReflect_getDeclaredConstructor(JNIEnv *env, jobject t,
                                                                     jclass c,
                                                                     jobjectArray params) {
    std::thread test_thread ;
    jobject global_clazz = env->NewGlobalRef(c) ;
    jobjectArray global_params = nullptr;
    int arg_length = env->GetArrayLength(params);
    if (params != nullptr) {
        for (int i = 0; i < arg_length; i++) {
            jobject element = static_cast<jobject> (env->GetObjectArrayElement(params, i));
            jobject global_element = env->NewGlobalRef(element);
            env->SetObjectArrayElement(params, i, global_element);
        }
        global_params = (jobjectArray) env->NewGlobalRef(params);
    }

    auto future = std::async(&getDeclaredConstructor_internal, global_clazz,global_params);
    auto result = future.get();

    if (global_params != nullptr) {
        env->DeleteGlobalRef(global_params) ;
    }

    return result ;
}