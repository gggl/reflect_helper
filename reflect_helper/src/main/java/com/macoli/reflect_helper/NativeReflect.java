package com.macoli.reflect_helper;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

class NativeReflect {
    static {
        System.loadLibrary("reflect_helper");
    }
    public native Constructor getDeclaredConstructor(Class<?> clz , Class<?>[] params) ;
    public native Method getDeclaredMethod(Class<?> clz , String methodName , Class<?>[] params) ;
    public native Field getDeclaredField(Class<?> clz, String name);

    private NativeReflect() {}

    public static NativeReflect getInstance(){
        return HOLDER.instance ;
    }

    private static class HOLDER {
        private static final NativeReflect instance = new NativeReflect() ;
    }

}
