# reflect_helper

#### 介绍
android反射被hide修饰的方法和字段，适配android11和android12。
#### 详解

https://blog.csdn.net/mldxs/article/details/129192644?csdn_share_tail=%7B%22type%22%3A%22blog%22%2C%22rType%22%3A%22article%22%2C%22rId%22%3A%22129192644%22%2C%22source%22%3A%22mldxs%22%7D

#### 安装教程

implementation 'com.gitee.gggl:reflect_helper:1.1'