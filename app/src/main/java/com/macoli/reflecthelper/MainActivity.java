package com.macoli.reflecthelper;

import androidx.appcompat.app.AppCompatActivity;

import android.net.wifi.p2p.WifiP2pManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CpuUsageInfo;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import com.macoli.reflect_helper.ReflectHelper;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.reflect_btn).setOnClickListener(v -> {
            try {

                 /** {@hide} */
                 /*
                public void setTraceTag(long traceTag) {
                    mTraceTag = traceTag;
                }
                * */
                Method m = ReflectHelper.getDeclaredMethod(Looper.class , "setTraceTag" , new Class[]{long.class}) ;
                m.invoke(Looper.getMainLooper() , 1000) ;
                Log.d("gggl" , "setTraceTag " + m.toString()) ;

                Class<?> activityThreadCls = Class.forName("android.app.ActivityThread") ;
                Field f = ReflectHelper.getDeclaredField(activityThreadCls , "TAG") ;
                Object tag = f.get(null) ;
                Log.d("gggl" , f.toString()) ;
                Log.d("gggl" , tag.toString()) ;
//                CpuUsageInfo cpuUsageInfo = new CpuUsageInfo() ;
                /** @hide */
                /*public CpuUsageInfo(long activeTime, long totalTime) {
                    mActive = activeTime;
                    mTotal = totalTime;
                }*/
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Constructor cpuUsageInfoConstructor = ReflectHelper.getDeclaredConstructor(CpuUsageInfo.class , new Class[]{long.class , long.class}) ;
                    Object cpuUsageObj = cpuUsageInfoConstructor.newInstance(100 , 100) ;
                    Method getTotalM = CpuUsageInfo.class.getDeclaredMethod("getTotal") ;
                    Object r = getTotalM.invoke(cpuUsageObj) ;
                    Log.d("gggl" , "getTotal = " + r) ;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}

/**
 * 06-19 15:15:35.181 14157 14157 D gggl    : setTraceTag public void android.os.Looper.setTraceTag(long)
 * 06-19 15:15:35.182 14157 14157 D gggl    : public static final java.lang.String android.app.ActivityThread.TAG
 * 06-19 15:15:35.182 14157 14157 D gggl    : ActivityThread
 * 06-19 15:15:35.182 14157 14157 D gggl    : getTotal = 100
 * */